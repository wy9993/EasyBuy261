package dao;

import java.util.List;

import entity.Product;
import util.PageUtil;

public interface ProductDao{
	/**
	 * 
	 * @param categoryId
	 * @param categoryLeveI
	 * @param starIndex起始索引
	 * @param pageSize页面大小
	 * @return
	 */
	
	PageUtil<Product> findProductsByCategoryId(int categoryId,int categoryLeveI,int starIndex,int pageSize);

	/**
	 * 查总记录数
	 * @param categoryId
	 * @param categoryLevel
	 * @return
	 */
    int findCountByCategoryId(int categoryId ,int categoryLevel);
    
    /**
     * 查询商品
     * @param categoryId
     * @return
     */
   // List<Product> findById(int categoryId);

	List<Product> findName(String keyWord);

	int count(int categrory, int type);

	List<Product> querylist(int categrory, int type, int stackindex, int pageSize);

	Product findByid(int id);
}
