package dao;

import entity.User;

public interface UserDao {
	int add(User user);

	User findUserByLoginName(String loginName);

}
