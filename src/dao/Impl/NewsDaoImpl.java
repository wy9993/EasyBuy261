package dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.BaseDao;
import dao.NewsDao;
import entity.News;

public class NewsDaoImpl extends BaseDao implements NewsDao{

	private Connection conn;
	private PreparedStatement pstmt;
	private ResultSet rs;
	@Override
	public List<News> findNewsList() {
		List<News> list = new ArrayList<>();
		try {
			conn = super.getConnection();
			String sql = "select id,title,createTime from easybuy_news order by createTime desc limit 0,5";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				News news = new News();
				news.setId(rs.getInt("id"));
				news.setTitle(rs.getString("title"));
				news.setCreateTime(rs.getDate("createTime"));
				list.add(news);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
        	super.closeAll(conn, pstmt, rs);
        }
		return list;
	}

}
