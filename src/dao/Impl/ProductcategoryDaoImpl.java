package dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import dao.BaseDao;
import dao.ProductCategoryDao;
import entity.ProductCategory;

public class ProductCategoryDaoImpl extends BaseDao implements ProductCategoryDao {
	private Connection conn;
	private PreparedStatement pstmt;
	private ResultSet rs;
	
	@Override
	public List<ProductCategory> findAll() {
		List<ProductCategory> list = new ArrayList<>();
		try {
			conn = super.getConnection();
			String sql = "select id,name,parentid,type,iconClass from easybuy_product_category";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				ProductCategory category = new ProductCategory();
				category.setId(rs.getInt("id"));
				category.setName(rs.getString("name"));
				category.setParentId(rs.getInt("parentid"));
				category.setType(rs.getInt("type"));
				category.setIconClass(rs.getString("iconClass"));
				list.add(category);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
        	super.closeAll(conn, pstmt, rs);
        }
		return list;
	}

	@Override
	public List<ProductCategory> List() {
		
		List<ProductCategory> list = new ArrayList<ProductCategory>();
		
		conn = super.getConnection();
		
		try {
			
			String sql = "select * from easybuy_product_category";
			
			pstmt = conn.prepareStatement(sql);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				ProductCategory product_category = new ProductCategory();
				
				product_category.setId(rs.getInt("id"));
				product_category.setName(rs.getString("name"));
				product_category.setParentId(rs.getInt("parentId"));
				product_category.setType(rs.getInt("type"));
				product_category.setIconClass(rs.getString("iconClass"));
				
				list.add(product_category);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			super.closeAll(conn, pstmt, rs);
		}

		return list;
	}
	
}
