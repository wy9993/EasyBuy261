package dao.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dao.BaseDao;
import dao.ProductDao;
import entity.Product;
import util.PageUtil;

public class ProductDaoImpl extends BaseDao implements ProductDao {
	Connection conn=null;
	PreparedStatement pstms=null;
	ResultSet rs =null;
	@Override
	public PageUtil<Product> findProductsByCategoryId(int categoryId, int categoryLevel,int starIndex,int pageSize) {
		PageUtil<Product> pageUtil = new PageUtil<Product>();
		pageUtil.setPageSize(pageSize);
		List<Product> list = new ArrayList<Product>();
		try {
			conn = super.getConnection();
			
			//查询内容数据
			String sql = "select id,name,description,price,stock,categoryLevel1Id,categoryLevel2Id,categoryLevel3Id,fileName,isDelete "
					+ " from easybuy_product where isDelete=0 ";
			
			switch (categoryLevel) {
			case 1:
				sql += "and categoryLevel1Id=? ";
				break;
			case 2:
				sql += "and categoryLevel2Id=? ";
				break;
			case 3:
				sql += "and categoryLevel3Id=? ";
				break;
			}
			
			sql += "limit ?,?";
			
			pstms = conn.prepareStatement(sql);
			pstms.setObject(1, categoryId);
			pstms.setObject(2, starIndex);
			pstms.setObject(3, pageSize);
			rs = pstms.executeQuery();
			
			while (rs.next()) {
				Product product = new Product();
		        product.setId(rs.getInt("id"));
		        product.setName(rs.getString("name"));
		        product.setDescription(rs.getString("description"));
		        product.setPrice(rs.getFloat("price"));
		        product.setStock(rs.getInt("stock"));
		        product.setCategoryLevel1Id(rs.getInt("categoryLevel1Id"));
		        product.setCategoryLevel2Id(rs.getInt("categoryLevel2Id"));
		        product.setCategoryLevel3Id(rs.getInt("categoryLevel3Id"));
		        product.setFileName(rs.getString("fileName"));
				list.add(product);
			}
			//将集合数据存到pageUtil里
			pageUtil.setList(list);
			
			int totalCount = 0;
			//查询记录数
			String sqlCount = "select count(1) "
					+ " from easybuy_product where isDelete=0 ";
			
			switch (categoryLevel) {
			case 1:
				sqlCount += "and categoryLevel1Id=? ";
				break;
			case 2:
				sqlCount += "and categoryLevel2Id=? ";
				break;
			case 3:
				sqlCount += "and categoryLevel3Id=? ";
				break;
			}
			
			pstms = conn.prepareStatement(sqlCount);
			pstms.setObject(1, categoryId);
			rs = pstms.executeQuery();
			
			if (rs.next()) {
				totalCount = rs.getInt(1);
			}
			pageUtil.setTotalCount(totalCount);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
        	super.closeAll(conn, pstms, rs);
        }
		return pageUtil;
	}

	@Override
	public int findCountByCategoryId(int categoryId, int categoryLevel) {
		int count=0;
		try {
			conn = super.getConnection();
			String sql = "select count(1) from easybuy_product where isDelete=0 ";	
					switch (categoryLevel) {
					case 1:
						sql += " and categoryLevel1Id=? ";
						break;
					case 2:
						sql += " and categoryLevel2Id=? ";
						break;
					case 3:
						sql += " and categoryLevel3Id=? ";
						break;
					}
					
			pstms = conn.prepareStatement(sql);
			pstms.setObject(1, categoryId);
			rs = pstms.executeQuery();
			if (rs.next()) {
				count=rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
        	super.closeAll(conn, pstms, rs);
        }
		return count;
	}
	
	
//	public List<Product> findById(int categoryId) {
//		List<Product> list = new ArrayList<Product>();
//		try {
//			conn=super.getConnection();
//			String sql="SELECT * from easybuy_product where isDelete=0 and id = ?";
//			
//			
//			pstms=conn.prepareStatement(sql);
//			
//			pstms.setObject(1,categoryId);
//			
//			rs=pstms.executeQuery();
//			while(rs.next()){
//				Product product=new Product();
//				product.setId(rs.getInt("id"));
//				product.setName(rs.getString("name"));
//				product.setDescription(rs.getString("description"));
//				product.setPrice(rs.getFloat("price"));
//				product.setCategoryLevel1Id(rs.getInt("categoryLevel1Id"));
//				product.setCategoryLevel2Id(rs.getInt("categoryLevel2Id"));
//				product.setCategoryLevel3Id(rs.getInt("categoryLevel3Id"));
//				product.setFileName(rs.getString("fileName"));
//				list.add(product);
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}finally {
//			super.closeAll(conn, pstms, rs);
//		}
//		return list;
//	}
	@Override
	public List<Product> findName(String keyWord) {
		List<Product> list = new ArrayList<Product>();
		
		try {
			
			conn = super.getConnection();
			
			String sql = "select * from easybuy_product where name Like '%"+keyWord+"%' ";		
			
			pstms = conn.prepareStatement(sql);
	
			rs = pstms.executeQuery();
			
			while (rs.next()) {
				Product product = new Product();
				
				product.setId(rs.getInt("id"));
				product.setFileName(rs.getString("fileName"));
				product.setCategoryLevel1Id(rs.getInt("categoryLevel1Id"));
				product.setCategoryLevel2Id(rs.getInt("categoryLevel2Id"));
				product.setCategoryLevel3Id(rs.getInt("categoryLevel3Id"));
				product.setDescription(rs.getString("description"));
				product.setIsDelete(rs.getInt("isDelete"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getFloat("price"));
				product.setStock(rs.getInt("stock"));
				
				list.add(product);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			super.closeAll(conn, pstms, rs);
		}
		
		return list;
	}
	@Override
	public int count(int categrory, int type) {
		int count = 0;
		try {
			
			conn = super.getConnection();
			
			String sql = "select count(1) as count from easybuy_product where isDelete = 0 and ";		
			
			if(type==1){
				sql += "categoryLevel1Id = ? ";
			}
			if(type==2){
				sql += "categoryLevel2Id = ? ";
			}
			if(type==3){
				sql += "categoryLevel3Id = ? ";
			}
			
			pstms = conn.prepareStatement(sql);
			pstms.setInt(1, categrory);
			
			rs = pstms.executeQuery();
			
			if (rs.next()) {
				count = rs.getInt("count");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			super.closeAll(conn, pstms, rs);
		}
		
		return count;
	}
	@Override
	public List<Product> querylist(int categrory, int type, int stackindex, int pageSize) {
		List<Product> list = new ArrayList<Product>();
		
		try {
			
			conn = super.getConnection();
			
			String sql = "select * from easybuy_product where isDelete = 0 and ";		
			
			if(type==1){
				sql += "categoryLevel1Id = ? ";
			}
			if(type==2){
				sql += "categoryLevel2Id = ? ";
			}
			if(type==3){
				sql += "categoryLevel3Id = ? ";
			}
			
			
			
			sql +=  "LIMIT ?,?";
			
			pstms = conn.prepareStatement(sql);
			
			pstms.setInt(1, categrory);
			pstms.setInt(2, stackindex);
			pstms.setInt(3, pageSize);
			
			rs = pstms.executeQuery();
			
			while (rs.next()) {
				Product product = new Product();
				
				product.setId(rs.getInt("id"));
				product.setFileName(rs.getString("fileName"));
				product.setCategoryLevel1Id(rs.getInt("categoryLevel1Id"));
				product.setCategoryLevel2Id(rs.getInt("categoryLevel2Id"));
				product.setCategoryLevel3Id(rs.getInt("categoryLevel3Id"));
				product.setDescription(rs.getString("description"));
				product.setIsDelete(rs.getInt("isDelete"));
				product.setName(rs.getString("name"));
				product.setPrice(rs.getFloat("price"));
				product.setStock(rs.getInt("stock"));
				
				list.add(product);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			super.closeAll(conn, pstms, rs);
		}
		
		return list;
	}

	@Override
	public Product findByid(int id) {
		Product product =null;
		try {
			conn = super.getConnection();
			String sql = "select * from easybuy_product where id =? ";	
			pstms = conn.prepareStatement(sql);
			pstms.setObject(1, id);
			rs = pstms.executeQuery();
			if(rs.next()){
				product =new Product();
				product.setId(rs.getInt("id"));
				product.setName(rs.getString("name"));
				product.setDescription(rs.getString("description"));
				product.setPrice(rs.getFloat("price"));
				product.setStock(rs.getInt("stock"));
				product.setCategoryLevel1Id(rs.getInt("categoryLevel1Id"));
				product.setCategoryLevel2Id(rs.getInt("categoryLevel2Id"));
				product.setCategoryLevel3Id(rs.getInt("categoryLevel3Id"));
				product.setFileName(rs.getString("fileName"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
        	super.closeAll(conn, pstms, rs);
        }
		return product;
	}
	
}


