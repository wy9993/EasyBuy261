package service.Impl;

import dao.UserDao;
import dao.Impl.UserDaoImpl;
import entity.User;
import service.UserService;
import util.SecurityUtils;

public class UserServiceImpl implements UserService {

	private UserDao dao=new UserDaoImpl();
	@Override
	public boolean register(User user) {
		
		// TODO Auto-generated method stub
		//加密操作
		String md5Pwd = SecurityUtils.md5Hex(user.getPassword());
		user.setPassword(md5Pwd);
		
			return dao.add(user)>0;
	}

	@Override
	public User login(String loginName, String password) {
		User user =dao .findUserByLoginName(loginName);
		if (user!=null) {//有这个账号的用户
			//判断密码
			String md5Pwd=SecurityUtils.md5Hex(password);
			if (md5Pwd.equals(user.getPassword())) {
				return user;
			}
		}
		return null;
	}
}

