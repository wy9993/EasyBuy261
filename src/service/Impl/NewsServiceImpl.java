package service.Impl;

import java.util.List;

import dao.NewsDao;
import dao.Impl.NewsDaoImpl;
import entity.News;
import service.NewsService;

public class NewsServiceImpl  implements NewsService {

	private NewsDao dao =new NewsDaoImpl();
	@Override
	public List<News> findNewsList() {
		
		return dao.findNewsList();
	}
	

}
