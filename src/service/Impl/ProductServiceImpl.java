package service.Impl;

import java.util.List;

import dao.ProductDao;
import dao.Impl.ProductDaoImpl;
import entity.Product;
import service.ProductService;
import util.PageUtil;

public class ProductServiceImpl implements ProductService {
	private ProductDao dao =new ProductDaoImpl();

    public PageUtil<Product> findProductsByCategoryId(int categoryId,int categoryLeveI,int currPage,int pageSize) {
		int startIndex=(currPage-1)*pageSize;
		return dao.findProductsByCategoryId(categoryId,categoryLeveI,startIndex,pageSize);
	}
	@Override
	public int findCountByCategoryId(int categoryId, int categoryLevel) {
		// TODO Auto-generated method stub
		return dao.findCountByCategoryId(categoryId, categoryLevel);
	}
//	@Override
//	public List<Product> findById(int categoryId) {
//		// TODO Auto-generated method stub
//		return dao.findById(categoryId);
//	}
	@Override
	public List<Product> findName(String keyWord) {
		// TODO Auto-generated method stub
		return dao.findName(keyWord);
	}
	@Override
	public int count(int categrory, int type) {
		
		return dao.count(categrory,type);
	}
	@Override
	public List<Product> querylist(int categrory, int type, int currpage, int pageSize) {
		 int stackindex= ( currpage  -1)*pageSize;
			
		return dao.querylist(categrory, type, stackindex, pageSize);
	}
	@Override
	public Product findById(int id) {
		
		return dao.findByid(id) ;
	}
	
}
