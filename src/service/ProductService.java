package service;

import java.util.List;

import entity.Product;
import util.PageUtil;

public interface ProductService {
	/**
	 * 
	 * @param categoryId
	 * @param categoryLeveI
	 * @param currPage当前页码
	 * @param pageSize页面大小
	 * @return
	 */
	PageUtil<Product> findProductsByCategoryId(int categoryId,int categoryLeveI,int currPage,int pageSize);
	
	
	List<Product> querylist(int categrory, int type, int currpage, int pageSize); 
	 
	int findCountByCategoryId(int categoryId ,int categoryLevel);
	/**
     * 查询商品
     * @param categoryId
     * @return
     */
	//List<Product> findById(int categoryId);

	List<Product> findName(String keyWord);

	int count(int categrory, int type);


	Product findById(int id);
	
	
	
}
