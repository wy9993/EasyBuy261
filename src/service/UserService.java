package service;

import entity.User;

public interface UserService {
	
	boolean register(User user);
	
	User login(String loginName,String password);

}
