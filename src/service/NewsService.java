package service;

import java.util.List;

import entity.News;

public interface NewsService {
	List<News> findNewsList();

}
