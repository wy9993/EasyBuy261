package util;

/**
 * 工具类
 * 封装返回结果的类
 * @author Aying·
 *
 */
public class ReturnResult {
	private int status;
	
	private String message="操作失败";

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public ReturnResult(int status,String message){
		this.status=status;
		this.message=message;
	}
	public ReturnResult(){
		
	}

}
