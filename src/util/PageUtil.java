package util;

import java.util.List;

public class PageUtil<T> {
	
	private int currPageNo = 1;//当前页码
	private int totalCount;//总条数
	private int pageSize = 8;//每页显示条数
	private int totalPage;//总页数
	
	private List<T> list; //数据
	
	
	
	
	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
	public int getCurrPageNo() {
		return currPageNo;
	}
	public void setCurrPageNo(int currPageNo) {
		this.currPageNo = currPageNo;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		
		this.totalPage = this.totalCount / pageSize;
		
		if (this.totalCount%pageSize>0) {
			this.totalPage+=1;
		}
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;

		this.totalPage = this.totalCount / pageSize;
		
		if (this.totalCount%pageSize>0) {
			this.totalPage+=1;
		}
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
	

}
