package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Product;
import entity.ProductCategory;
import service.ProductCategoryService;
import service.ProductService;
import service.Impl.ProductCategoryServiceImpl;
import service.Impl.ProductServiceImpl;
import util.PageUtil;

public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	private ProductService productService = new ProductServiceImpl();
	
	private ProductCategoryService productCategoryService = new ProductCategoryServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String action = request.getParameter("action");
		if (action != null && action.length()>0) {
			switch (action) {
			case "queryList":
				queryList(request,response);
				break;
			case "queryDeatil":
				queryDetails(request,response);
				break;
			default:
				response.sendRedirect("404.jsp");
				break;
			}
		} else {
			response.sendRedirect("404.jsp");
		}	
	}
	private void queryDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id =Integer.parseInt(request.getParameter("id"));
		
		Product product =	productService.findById(id);
		
		request.setAttribute("product", product);
		List<ProductCategory> allProductCategrory = productCategoryService.List();
		request.setAttribute("allProductCategrory", allProductCategrory);
		request.getRequestDispatcher("pre/productDeatil.jsp").forward(request, response);

	}
	private void queryList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryId = request.getParameter("categoryId");
		
		String categoryLevel = request.getParameter("categoryLevel");
		
		int currPageNo = Integer.parseInt(request.getParameter("currPageNo"));
		
		int pageSize = 4;
		
		PageUtil<Product> pageUtil = productService.findProductsByCategoryId(Integer.parseInt(categoryId),Integer.parseInt(categoryLevel),currPageNo,pageSize);
		
		pageUtil.setCurrPageNo(currPageNo);
		//listProduct
		request.setAttribute("pageUtil", pageUtil);
		//获取菜单
		List<ProductCategory> listCategory = productCategoryService.findAll();
		
		request.setAttribute("allProductCategrory", listCategory);
		
		//获取1级菜单中的商品,将其放入map中
		Map<Integer, List<Product>> mapProduct = new HashMap<Integer, List<Product>>();
		
		for (ProductCategory productCategory : listCategory) {
			if (productCategory.getType()==1) {
				List<Product> listProduct = productService.findProductsByCategoryId(productCategory.getId(),1,1,6).getList();
				
				mapProduct.put(productCategory.getId(), listProduct);
			}
		}
		
		//将该分类下的商品存在request作用域
		request.setAttribute("mapProductsOfCategory", mapProduct);
		
		request.getRequestDispatcher("pre/queryProductList.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
