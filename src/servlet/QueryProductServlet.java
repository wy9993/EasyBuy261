package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Product;
import entity.ProductCategory;
import service.ProductCategoryService;
import service.ProductService;
import service.Impl.ProductCategoryServiceImpl;
import service.Impl.ProductServiceImpl;
import util.PageUtil;

/**
 * Servlet implementation class QueryProductServlet
 */
public class QueryProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	private static ProductService service = new ProductServiceImpl();
	private static ProductCategoryService service1 = new ProductCategoryServiceImpl();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		String action = request.getParameter("action");


		switch (action) {
		
		case "Query":
			Query(request,response);
			break;
		case "queryProductList":
			queryProductList(request,response);
			break;
		default:
			break;
		}
		
		out.close();
	}

	private void queryProductList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		String keyWord = request.getParameter("keyWord");
		
		List<Product> product =  service.findName(keyWord);
		
		List<ProductCategory> allProductCategrory = service1.List();
		request.setAttribute("allProductCategrory", allProductCategrory);
		request.setAttribute("product", product);
		request.getRequestDispatcher("pre/queryProductList.jsp").forward(request, response);
		
		out.close();
		
	}

	private void Query(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		int categrory = Integer.parseInt(request.getParameter("categoryId"));
		int type = Integer.parseInt(request.getParameter("categoryLevel"));
		
		int currpage = Integer.parseInt(request.getParameter("currPageNo"));
		int pageSize = 4;
		
		int totalcount = service.count(categrory, type);//总条数
		
		
		List<Product> list = service.querylist(categrory, type,currpage,pageSize);
		
		PageUtil<Product> pager = new PageUtil<Product>();
		
		pager.setCurrPageNo(currpage);
		pager.setPageSize(pageSize);
		pager.setTotalCount(totalcount);
		pager.setList(list);
		
		
		List<ProductCategory> allProductCategrory = service1.List();
		request.setAttribute("allProductCategrory", allProductCategrory);
		
		request.setAttribute("pageUtil", pager);
		
		
		request.getRequestDispatcher("pre/queryProductList.jsp").forward(request, response);
		
		out.close();
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}


}
