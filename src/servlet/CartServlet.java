package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alibaba.fastjson.JSON;

import entity.Product;
import service.ProductService;
import service.Impl.ProductServiceImpl;
import util.ReturnResult;
import util.ShoppingCart;
import util.ShoppingCartItem;

public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   private ProductService productService =new ProductServiceImpl();
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		String action = request.getParameter("action");
		if (action != null && action.length()>0) {
			switch (action) {
			case "add":
				add(request,response);
				break;
			case "toSettlement":
				toSettlement(request,response);
				break;
			case "settlement1":
				settlement1(request,response);
				break;
			case "modCart":
				modCart(request,response);
				break;
			default:
				response.sendRedirect("404.jsp");
				break;
			}
		} else {
			response.sendRedirect("404.jsp");
		}	
	}
	
	private void modCart(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int productId = Integer.parseInt(request.getParameter("entityId"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		
		ShoppingCart cart = getShoppingCartFromSession(request);
		if(quantity<=0){
			 //删除
			 cart.items.remove(productId);
		 }else{
			//修改购物车某个商品的数量
			ShoppingCartItem cartItem =	cart.getItems().get(productId);
			cartItem.setQuantity(quantity);
		 }
		//计算总金额
		 cart.calSum();
		 
		 request.getSession().setAttribute("cart", cart);
		 
		 
		//计算总金额
		cart.calSum();
		
		request.getSession().setAttribute("cart",cart );
		PrintWriter out =response.getWriter();
		ReturnResult result =new ReturnResult();
		result.setMessage("操作成功");
		result.setStatus(1);
		

		String jsonstr=JSON.toJSONString(result);
		out.print(jsonstr);
		out.flush();
		out.close();
	}

	private void settlement1(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("pre/settlement/settlement1.jsp").forward(request, response);
		
	}

	private void toSettlement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("pre/settlement/toSettlement.jsp").forward(request, response);
	}

	private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		PrintWriter out =response.getWriter();
		
		//获取参数
		int productId =Integer.parseInt(request.getParameter("entityId"));
		int quantity=Integer.parseInt(request.getParameter("quantity"));
		
		//获取product
		Product product	= productService.findById(productId);
		
		//先去session哪购物车
		HttpSession session =request.getSession();
		ShoppingCart cart=getShoppingCartFromSession(request);
		//将商品放入购物车（计算总额）
		cart.addItem(product, quantity);
		
		
		session.setAttribute("cart", cart);
		//响应
		ReturnResult result =new ReturnResult(1,"已加入购物车");
		
		String jsonstr=JSON.toJSONString(result);
		out.print(jsonstr);
		out.flush();
		out.close();
		
		
	}
	private ShoppingCart getShoppingCartFromSession(HttpServletRequest request){
		HttpSession session =request.getSession();
		ShoppingCart cart=(ShoppingCart) session.getAttribute("cart");
		
		if (cart ==null) {
			cart =new ShoppingCart();
		}
		return cart;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
