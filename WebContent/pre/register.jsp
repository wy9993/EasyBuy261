<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@ include file="/common/pre/header.jsp" %>
    <title>易买网</title>
    
	<script type="text/javascript">
		$(function(){

			$('form').submit(function(){
				//获取相关字段的值
			    var loginName = $("input[name='loginName']").val();
			    var userName = $("input[name='userName']").val();
			    var password = $("input[name='password']").val();
			    var confirmPassword = $("input[name='confirmPassword']").val();
			    var email = $("input[name='email']").val();
			    var mobile = $("input[name='mobile']").val();
			    var identityCode = $("input[name='identityCode']").val();
			    var address = $("input[name='address']").val();
			    var sex = $("input[name='sex']:checked").val();
			    //判断密码是否一致
			    if(loginName==null || loginName==""){
			        showMessage("用户名不能为空.");
			        return false;
			    }

			    if(loginName.length<2 || loginName.length>10){
			        showMessage("登录名不能小于两个字符或者大于十个字符.");
			        return false;
			    }

			    //判断密码是否为空
			    if (password =="") {
			        showMessage("密码不能为空");
			        return false;
			    }

			    if (password != confirmPassword) {
			        showMessage("两次输入的密码不一致.");
			        return false;
			    }

			    if(userName==null || userName==""){
			        showMessage("真实姓名不能为空.");
			        return false;
			    }

			    if(userName.length<2 || userName.length>10){
			        showMessage("真实姓名不能小于两个字符或者大于十个字符.");
			        return false;
			    }
			    
			    //验证邮箱格式
			    if(email!=null && email!="" && !checkMail(email)){
			    	showMessage("邮箱格式不正确");
			    	return false;
			    }
			    //验证邮箱格式
			    if(mobile!=null && mobile!="" && !checkMobile(mobile)){
			    	showMessage("手机格式不正确");
			    	return false;
			    }
			     //验证邮箱格式
			    if(identityCode!=null && identityCode!="" && !checkIdentityCode(identityCode)){
			    	showMessage("身份证号格式不正确");
			    	return false;
			    }
			});
			
		});

		function register() {

			if(checkForm()){
				var formData = $('form').serialize();
				console.log(formData);
				//发送Ajax请求
				$.ajax({
			        url: "${ctx}/register?action=doRegister",
			        method: "post",
			        data: formData,
			        dataType:"json",
			        success: function (jsonResult) {
			        	showMessage(jsonResult.message);
			            if (jsonResult.status == 1) {
			                window.location.href = contextPath + "/login?action=toLogin";
			            } 
			        }
			    });
			}

		}

		function checkForm(){

			//获取相关字段的值
		    var loginName = $("input[name='loginName']").val();
		    var userName = $("input[name='userName']").val();
		    var password = $("input[name='password']").val();
		    var confirmPassword = $("input[name='confirmPassword']").val();
		    var email = $("input[name='email']").val();
		    var mobile = $("input[name='mobile']").val();
		    var identityCode = $("input[name='identityCode']").val();
		    var address = $("input[name='address']").val();
		    var sex = $("input[name='sex']:checked").val();
		    //判断密码是否一致
		    if(loginName==null || loginName==""){
		        showMessage("用户名不能为空.");
		        return false;
		    }

		    if(loginName.length<2 || loginName.length>10){
		        showMessage("登录名不能小于两个字符或者大于十个字符.");
		        return false;
		    }

		    //判断密码是否为空
		    if (password =="") {
		        showMessage("密码不能为空");
		        return false;
		    }

		    if (password != confirmPassword) {
		        showMessage("两次输入的密码不一致.");
		        return false;
		    }

		    if(userName==null || userName==""){
		        showMessage("真实姓名不能为空.");
		        return false;
		    }

		    if(userName.length<2 || userName.length>10){
		        showMessage("真实姓名不能小于两个字符或者大于十个字符.");
		        return false;
		    }
		    
		    //验证邮箱格式
		    if(email!=null && email!="" && !checkMail(email)){
		    	showMessage("邮箱格式不正确");
		    	return false;
		    }
		    //验证邮箱格式
		    if(mobile!=null && mobile!="" && !checkMobile(mobile)){
		    	showMessage("手机格式不正确");
		    	return false;
		    }
		     //验证邮箱格式
		    if(identityCode!=null && identityCode!="" && !checkIdentityCode(identityCode)){
		    	showMessage("身份证号格式不正确");
		    	return false;
		    }

		    return true;
		}

		function checkMail(mail) {
		  var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  if (filter.test(mail)) 
			  return true;
		  return false;
		}

		function checkMobile(phone) {
		  var filter  = /^\d{5,11}$/;
		  if (filter.test(phone)) 
			  return true;
		  return false;
		}

		function checkIdentityCode(identityCode) {
		  var filter  = /^\w{18}$/;
		  if (filter.test(identityCode)) 
			  return true;
		  return false;
		}
	</script>
</head>
<body>
<!--Begin Login Begin-->
<div class="log_bg">
    <div class="top">
        <div class="logo"><a href="${ctx}/Home?action=index"><img src="${ctx}/statics/images/logo.png"/></a></div>
    </div>
    <div class="regist">
        <div class="log_img"><img src="${ctx}/statics/images/l_img.png" width="611" height="425"/></div>
        <div class="reg_c">
            <form id="register">
                <table border="0" style="width:420px; font-size:14px; margin-top:20px;" cellspacing="0" cellpadding="0">
                    <tr height="50" valign="top">
                        <td width="95">&nbsp;</td>
                        <td>
                            <span class="fl" style="font-size:24px;">注册</span>
                            <span class="fr">已有商城账号，<a href="${ctx}/Login?action=toLogin" style="color:#ff4e00;">我要登录</a></span>
                        </td>
                    </tr>
                    <tr height="50">
                        <td align="right"><font color="#ff4e00">*</font>登录用户名 &nbsp;</td>
                        <td><input type="text" value="" name="loginName" class="l_user"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right"><font color="#ff4e00">*</font>&nbsp;密码 &nbsp;</td>
                        <td><input type="password" value="" name="password" class="l_pwd"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right"><font color="#ff4e00">*</font>&nbsp;确认密码 &nbsp;</td>
                        <td><input type="password" value="" name="confirmPassword" class="l_pwd"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right"><font color="#ff4e00">*</font>&nbsp;真实姓名 &nbsp;</td>
                        <td><input type="text" value="" name="userName" class="l_user"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right"><font color="#ff4e00">*</font>&nbsp;性别 &nbsp;</td>
                        <td>
                            <input type="radio" name="sex" value="1" checked="checked">&nbsp;男&nbsp;&nbsp;
                            <input type="radio" name="sex" value="0">&nbsp;女
                        </td>
                    </tr>

                    <tr height="50">
                        <td align="right">&nbsp;身份证号 &nbsp;</td>
                        <td><input type="text" value="" name="identityCode" class="l_user"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right">&nbsp;邮箱 &nbsp;</td>
                        <td><input type="text" value="" name="email" class="l_email"/></td>
                    </tr>
                    <tr height="50">
                        <td align="right">&nbsp;手机 &nbsp;</td>
                        <td><input type="text" value="" name="mobile" class="l_tel"/></td>
                    </tr>
                    <tr height="60">
                        <td>&nbsp;</td>
                        <td><input type="button" value="立即注册" class="log_btn" onclick="register()"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<!--End Login End-->
<!--Begin Footer Begin-->
<div class="btmbg">
    <div class="btm">
        备案/许可证编号：蜀ICP备12009302号-1-www.dingguagua.com Copyright © 2015-2018 尤洪商城网 All Rights Reserved. 复制必究 , Technical
        Support: Dgg Group <br/>
        <img src="${ctx}/statics/images/b_1.gif" width="98" height="33"/><img src="${ctx}/statics/images/b_2.gif"  width="98" height="33"/><img
            src="${ctx}/statics/images/b_3.gif" width="98" height="33"/><img src="${ctx}/statics/images/b_4.gif"   width="98" height="33"/><img
            src="${ctx}/statics/images/b_5.gif" width="98" height="33"/><img src="${ctx}/statics/images/b_6.gif"   width="98" height="33"/>
    </div>
</div>
<!--End Footer End -->
</body>
</html>
