<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <%@ include file="/common/pre/header.jsp" %>
    <title>易买网</title>
</head>
<body>
<!--Begin Header Begin-->
<%@ include file="/common/pre/searchBar.jsp" %>
<!--End Header End-->
<!--Begin Menu Begin-->
<div class="menu_bg">
	<div class="menu">
		<!-- 左边 -->
        <!--Begin 商品分类详情 Begin-->
        <%@ include file="/common/pre/categoryBar.jsp" %>
      	  <!--End 商品分类详情 End-->
       </div>
    </div>
<!--End Menu End-->
<div class="i_bg bg_color">
    <div class="i_ban_bg">
        <!--Begin Banner Begin-->
        <div class="banner">
            <div class="top_slide_wrap">
                <ul class="slide_box bxslider">
                    <li><img src="${ctx}/statics/images/ban1.jpg" width="740" height="401"/></li>
                    <li><img src="${ctx}/statics/images/ban1.jpg" width="740" height="401"/></li>
                    <li><img src="${ctx}/statics/images/ban1.jpg" width="740" height="401"/></li>
                </ul>
                <div class="op_btns clearfix">
                    <a href="#" class="op_btn op_prev"><span></span></a>
                    <a href="#" class="op_btn op_next"><span></span></a>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var jq2 = jQuery.noConflict();
            (function () {
                $(".bxslider").bxSlider({
                    auto: true,
                    prevSelector: jq2(".top_slide_wrap .op_prev")[0], nextSelector: jq2(".top_slide_wrap .op_next")[0]
                });
            })();
        </script>
        <!--End Banner End-->
        <div class="inews">
            <div class="news_t">
                <span class="fr"><a href="${ctx}/admin/news?action=queryNewsList">更多 ></a></span>新闻资讯
            </div>
            <ul>
                <c:forEach items="${newsList}" var="temp">
                    <li><span>[ 公告 ]</span>
                        <a href="${ctx}/admin/news?action=newsDeatil&id=${temp.id}">${temp.title}</a>
                    </li>
                </c:forEach>
            </ul>
            <div class="charge_t">
                话费充值
                <div class="ch_t_icon"></div>
            </div>
            <form>
                <table border="0" style="width:205px; margin-top:10px;" cellspacing="0" cellpadding="0">
                    <tr height="35">
                        <td width="33">号码</td>
                        <td><input type="text" value="" class="c_ipt"/></td>
                    </tr>
                    <tr height="35">
                        <td>面值</td>
                        <td>
                            <select class="jj" name="city">
                                <option value="0" selected="selected">100元</option>
                                <option value="1">50元</option>
                                <option value="2">30元</option>
                                <option value="3">20元</option>
                                <option value="4">10元</option>
                            </select>
                            <span style="color:#ff4e00; font-size:14px;">￥99.5</span>
                        </td>
                    </tr>
                    <tr height="35">
                        <td colspan="2"><input type="submit" value="立即充值" class="c_btn"/></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <div class="content mar_20">
        <img src="${ctx}/statics/images/mban_1.jpg" width="1200" height="110"/>
    </div>
    <!--Begin 楼层    Begin-->
    <c:set var="floor" value="0"></c:set>
    <c:forEach var="categrory1" items="${allProductCategrory }">
       	<c:if test="${categrory1.type == 1 }">
       		<!-- 显示一级菜单 -->
       		<c:set var="floor" value="${floor+1 }"></c:set>
		    <div class="i_t mar_10">
		    	<span class="floor_num">${floor }F</span>
		    	<span class="fl">${categrory1.name }</span>                
		    </div>
		    <div class="content">
		    	<div class="fresh_left">
		        	<div class="fre_ban">
		            	<div id="imgPlay1">
		                    <ul class="imgs" id="actor1" style="width: 633px; margin-left: 0px;">
		                        <li><a href="#"><img src="${ctx}/statics/images/fre_r.jpg" width="211" height="286"/></a></li>
	                            <li><a href="#"><img src="${ctx}/statics/images/fre_r.jpg" width="211" height="286"/></a></li>
	                            <li><a href="#"><img src="${ctx}/statics/images/fre_r.jpg" width="211" height="286"/></a></li>
		                    </ul>
		                    <div class="prevf">上一张</div>
		                    <div class="nextf">下一张</div> 
		                </div>   
		            </div>
		            <div class="fresh_txt">
		            	<div class="fresh_txt_c">
		            		<!-- 显示二级菜单 -->
		            		<c:forEach var="categrory2" items="${allProductCategrory }">
                           		<c:if test="${categrory2.type == 2 and categrory2.parentId==categrory1.id }">
                           			<a href="${ctx}/product?action=queryList&categoryId=${categrory2.id}&categoryLevel=${categrory2.type}&currPageNo=1">${categrory2.name }</a>
                           		</c:if>
	                        </c:forEach>
		                </div>
		            </div>
		        </div>
		        <div class="fresh_mid">
		        	<ul>
		        		<c:forEach var="product" items="${mapProductsOfCategory[categrory1.id]}">
		        			<li>
			                	<div class="name"><a href="javascript:void(0)">${product.name }</a></div>
			                    <div class="price">
			                    	<font>￥<span>${product.price }</span></font> &nbsp; 
			                    </div>
			                    <div class="img"><a href="${ctx}/product?action=queryDeatil&id=${product.id}"><img src="${ctx}/files/${product.fileName}" width="185" height="155"></a></div>
			                </li>
		        		</c:forEach>
		            </ul>
		        </div>
		        <div class="fresh_right">
		        	<ul>
		            	<li><a href="#"><img src="${ctx}/statics/images/fre_b1.jpg" width="260" height="220"/></a></li>
                    	<li><a href="#"><img src="${ctx}/statics/images/fre_b2.jpg" width="260" height="220"/></a></li>
		            </ul>
		        </div>
		    </div>
    	</c:if>
    </c:forEach>
    <!--End 楼层  End-->
    
    <%@ include file="/common/pre/footer.jsp" %>
</div>
</body>
</html>