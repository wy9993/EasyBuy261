<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript" src="${ctx}/statics/js/common/jquery-1.8.2.min.js"></script>
<script type="text/javascript">
    var contextPath = "${ctx}";
</script>
<!-- 左边 -->
   <div class="nav">
            <div class="nav_t">全部商品分类</div>
            <div class="leftNav">
                <ul>
               	<c:forEach var="categrory1" items="${allProductCategrory }">
	            	<c:if test="${categrory1.type == 1 }">
	            		<li>
	            			<!-- 一级菜单 -->
	            			<div class="fj">
	                        	<span class="n_img"><span></span><img src="${ctx}/statics/images/nav2.png"></span>
	                            <span class="fl">${categrory1.name }</span>
	                        </div>
	                        
	                        <div class="zj" style="display: none;">
	                            <div class="zj_l">
	                            	<!-- 二级菜单 -->
	                            	<c:forEach var="categrory2" items="${allProductCategrory }">
	                            		<c:if test="${categrory2.type == 2 and categrory2.parentId==categrory1.id }">
	                            			<div class="zj_l_c">
                                    			<h2>
                                    				<a href="${ctx}/query?action=Query&categoryId=${categrory2.id}&categoryLevel=${categrory2.type}&currPageNo=1">${categrory2.name }</a>
                                    			</h2>
                                    			<c:forEach var="categrory3" items="${allProductCategrory }">
	                            					<c:if test="${categrory3.type == 3 and categrory3.parentId==categrory2.id }">
	                            						<a href="${ctx}/query?action=Query&categoryId=${categrory3.id}&categoryLevel=${categrory3.type}&currPageNo=1">${categrory3.name }</a> &nbsp; &nbsp;
	                            					</c:if>
	                            				</c:forEach>
                                    		</div>
	                            		</c:if>
	                            	</c:forEach>
	                            </div>
                            </div>
	            		</li>
	            	</c:if>
	            </c:forEach>   
                </ul>            
            </div>
        </div>
        <!-- 中间 -->
        <ul class="menu_r">                                                                                                                                               
        	<li><a href="${ctx}/home?action=index">首页</a></li>
            <c:forEach var="categrory" items="${allProductCategrory }">
            	<c:if test="${categrory.type == 1 }">
            		<li><a href="${ctx}/query?action=Query&categoryId=${categrory.id}&categoryLevel=${categrory.type}&currPageNo=1">${categrory.name }</a></li>
            	</c:if>
            </c:forEach>
        </ul>
        <!-- 右边  -->
        <div class="m_ad">中秋送好礼！</div>